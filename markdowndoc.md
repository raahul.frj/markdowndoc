# Trying out Markdown guide
## This file contains some of these markdown standards
### Done by Santhiya
#### These are all headings and goes up to h6
Now let's try out wrapping text.
The output will be these lines joined together.<br>
This is another line and another paragragh

let's try it out.

For additional breaks we use 

<br>

this gives extra spaces

To get Horizontal lines we usually use 4 lines

----
like this.

I can use bold like, **Hello I'm Santhiya** and italic font like, _this is a sample work_

To use both font style together, ***Nice meeting you***


Here is a inline link here, [about.markdown.guidelines](https://about.gitlab.com/handbook/markdown-guide/)

Here is a relative link,[my.page](/santhiyas0907/markdowndoc/)

If you have queries, [santhiyas0907@gitlab.com](mailto:santhiyas0907@gitlab.com)
### More about myself:
- List of my skills,

  1.  Soft skills

      - Communication

      - Team work

      - Time Management

      - Problem solving

  1.  Technical skills

      - Html
      - CSS
      - Python
      - mysql

- Education
  
  1. SSN college of Engineering

  1. AGM School

![Hello-image.png](./Hello-image.png) *I can insert the image*

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/enMumwvLAug" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->





























[def]: /images/path/to/folder/image.png "Hello World"